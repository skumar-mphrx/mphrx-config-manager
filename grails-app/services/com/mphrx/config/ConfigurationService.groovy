package com.mphrx.config

import com.mphrx.Enum.ConfigDataType
import grails.transaction.Transactional
import groovy.json.JsonSlurper
import org.springframework.context.i18n.LocaleContextHolder as LCH
import sun.security.krb5.Config
import grails.converters.JSON


class ConfigurationService {

    def grailsApplication
    def messageSource

    /**
     * updates config in db and reload that config .
     * @param configProperties ["id":123,"fully qualified config key" : "value","fully qualified config key" : "value"]
     * @return
     */

    def update(Map configProperties) {

        Map responseText = ["msg": "Success", status: 200, httpStatusCode: 200]
        AppConfig appConfig = AppConfig.get(configProperties.id)
        List<ConfigMapBO> configMapList = appConfig.configMapList
        configMapList.each {
            def value = configProperties.get(it.configkey.replaceAll("\\.", "_"))
            if (value && it.configValue != value) {
                log.info("config-manager : key : ${it.configkey} oldValue : ${it.configValue}  newValue : ${value} .")
                it.oldValues.add([value: it.configValue.toString(), updateTime: new Date()])
                if (it.dataType == ConfigDataType.List.value || it.dataType == ConfigDataType.ValueSet.value) {
                    it.configValueList = value
                } else if (it.dataType == ConfigDataType.Map.value) {
                    it.configValueMap = value
                } else {
                    it.configValue = value
                }
            }
        }

        responseText=loadAppConfig(appConfig)
        appConfig.save(flush: true)
        return responseText

    }

    /**
     * Reads all configs from DB and loads into memory.
     */
    def loadAllConfigs() {
        List<AppConfig> appConfigs = AppConfig.list()
        appConfigs.each { appConfig ->
            loadAppConfig(appConfig)
        }
    }

    /**
     * Loads all keys of appconfig into memory.
     * @param appConfig
     * @return
     */
    Map loadAppConfig(AppConfig appConfig) {
        Map responseText = ["msg": "Success", status: 200, httpStatusCode: 200]
        try {
            ConfigObject configObject = grailsApplication.config
            ConfigSlurper configSlurper = new ConfigSlurper()
            appConfig.configMapList.each {
                log.info("Loading config ... ${it.configkey} = ${it.configValue} dataType = ${it.dataType}.")
                if (it.dataType == ConfigDataType.Closure.value) {
                    String clr = """
                          ${it.configkey}=${it.configValue}
                    """
                    configObject.merge(configSlurper.parse(clr))
                } else if (it.dataType == ConfigDataType.ValueSet.value) {
                    def jsonString = it.configValueList as JSON
                    jsonString = jsonString.toString().replace('{', '[').replace('}', ']')
                    configObject.merge(configSlurper.parse("${it.configkey}=${jsonString} "))
                } else if (it.dataType == ConfigDataType.List.value) {
                    def jsonString = it.configValueList as JSON
                    configObject.merge(configSlurper.parse("${it.configkey}=${jsonString} "))
                } else if (it.dataType == ConfigDataType.Map.value) {
                    def jsonString = it.configValueMap as JSON
                    jsonString = jsonString.toString().replace('{', '[').replace('}', ']')
                    configObject.merge(configSlurper.parse("${it.configkey}=${jsonString} "))
                } else if (it.dataType == ConfigDataType.Integer.value) {
                    configObject.merge(configSlurper.parse("${it.configkey}=${it.configValue} "))
                } else if (it.dataType == ConfigDataType.Float.value) {
                    configObject.merge(configSlurper.parse("${it.configkey}=${it.configValue} "))
                } else {
                    configObject.merge(configSlurper.parse("${it.configkey}='${it.configValue}' "))
                }
            }
        } catch (Exception ex) {
            log.error("Exception occured in ${appConfig.groupName} load .............")
            log.error(ex.printStackTrace())
            responseText = ["msg": "Exception", status: 500, httpStatusCode: 500]
        }
        return responseText
    }

    /**
     * Reads configJson file from disk and load configs into DB . This method should be called at the time of first deployment or any new config is created .
     * This methods adds/insert new configs into DB.
     * This method checks that if config is already there then skips means does not update the existing config . Existing config's value can be updated form UI only.
     *
     */

    def loadConfigIntoDB() {

        File configFileInJson = new File("/home/shaurav/gitProjects/mphrx-config-manager/grails-app/conf/BaseConfig.json")

        def jsonSlurper = new JsonSlurper()
        def configs = jsonSlurper.parse(configFileInJson)

        configs.each { Map configProperties ->

            AppConfig appConfig = AppConfig.findByGroupName(configProperties.groupName)
            if (appConfig) {
                log.info("Found old config ...${configProperties.groupName} skipping ...")
            } else {
                appConfig = new AppConfig()
                createOrUpdateConfigFromFile(appConfig, configProperties)
                log.info("Added new config ...${configProperties.groupName} .")
            }
        }
    }

    /**
     * Reads configJson file from disk and load configs into DB .
     * This method checks that if config is already there then updates the existing config with file content else insert a new one .
     *
     */

    def resetAllConfig() {

        File configFileInJson = new File("/home/shaurav/gitProjects/mphrx-config-manager/grails-app/conf/BaseConfig.json")

        def jsonSlurper = new JsonSlurper()
        def configs = jsonSlurper.parse(configFileInJson)

        configs.each { Map configProperties ->

            AppConfig appConfig = AppConfig.findByGroupName(configProperties.groupName)
            if (appConfig) {
                log.info("Found old config ...${configProperties.groupName} updating ...")
            } else {
                appConfig = new AppConfig()
                log.info("Adding new config ...${configProperties.groupName} .")
            }
            createOrUpdateConfigFromFile(appConfig, configProperties)
        }
    }

    def createOrUpdateConfigFromFile(AppConfig appConfig, Map configProperties) {
        appConfig.groupName = configProperties.groupName
        appConfig.groupNameLabelCode = configProperties.groupNameLabelCode
        appConfig.logicalGroupLabelCode = configProperties.logicalGroupLabelCode
        if (configProperties.logicalSubGroupLabelCode) {
            appConfig.logicalSubGroupLabelCode = configProperties.logicalSubGroupLabelCode
        }
        if (configProperties.logicalSubGroupDescriptionLabelCode) {
            appConfig.logicalSubGroupDescriptionLabelCode = configProperties.logicalSubGroupDescriptionLabelCode
        }
        appConfig.description = configProperties.description

        List configMapList = []

        configProperties.configMapList.each {
            ConfigMapBO configMapBO
            configMapBO = new ConfigMapBO(configkey: it.key, configValue: it.value, keyLabelCode: it.keyLabelCode, description: it.description, defaultValue: it.value, dataType: it.dataType)
            if (it.dataType == ConfigDataType.List.value || it.dataType == ConfigDataType.ValueSet.value) {
                configMapBO.configValueList = it.value
            } else if (it.dataType == ConfigDataType.Map.value) {

                configMapBO.configValueMap = it.value
            } else {
                configMapBO.configValue = it.value
            }
            configMapList << configMapBO
        }
        appConfig.configMapList = configMapList
        appConfig.save()
    }


}