<%--
  Created by IntelliJ IDEA.
  User: shaurav
  Date: 29/6/15
  Time: 11:07 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>

<html ng-app="configManagerApp">
<head>
    <title>Config Manager</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.1/angular-route.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/angular-local-storage/0.2.2/angular-local-storage.min.js"></script>

    <g:javascript src="configManager.js"/>

    <g:javascript src="jquery.js"/>


    %{--<g:javascript src="angular.min.js"/>--}%
    %{--<g:javascript src="angular-route.min.js"/>--}%

</head>

<body>


<div ng-view></div>


</body>
</html>