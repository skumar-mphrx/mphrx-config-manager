package com.mphrx.config

import org.springframework.context.i18n.LocaleContextHolder as LCH

/*
*  This domain is created for storing config's in database that admin can modify.
*  All config's are categorised in several logical groups and sublogical groups.  For example mail related config's are under "Mail" logical group .
*  Each config is stored by its fully qualified name . For example mail config keys are grails.mail.port , grails.mail.username
*
* */

class AppConfig {


    // one group can contain multiple key value pair , for ex: resertPassword group can contain subject and body
    String groupName

    // message.properties entry of groupName
    String groupNameLabelCode

    // description of group
    String description

    // Each config must be under some logical group and logicalGroupLabelCode contains code of message.properties of logical group label name
    String logicalGroupLabelCode

    // some config may comes under some sub logical group like modalities comes under list subgroup and list comes under system logical group.
    String logicalSubGroupLabelCode

    // message.properties entry of sub logical group description
    String logicalSubGroupDescriptionLabelCode

    // should be combination of appname and uniqueId ex: minerva_01,dicr_02
    List<String> clusterIds = []


    // map contains key,value ,default value etc.
    List<ConfigMapBO> configMapList

    // this config is used in which application's like MINERVA,DICR,CONSUS,ANGULAR
    List<String> appTags =["MINERVA"]

    Date dateCreated
    Date lastUpdated

    static embedded = ['configMapList']
    static mapWith = "mongo"
    static mapping = {
        version false
    }

    static constraints = {
        groupNameLabelCode(nullable: true)
        description(nullable: true)
        logicalSubGroupLabelCode(nullable: true)
        logicalSubGroupDescriptionLabelCode(nullable: true)
    }

    def formattedOutput() {
        return [
                id                   : id,
                groupName            : groupName,
                groupNameLabelCode   : groupNameLabelCode,
                logicalGroupLabelCode: logicalGroupLabelCode,
                configMapList        : configMapList
        ]
    }
}
