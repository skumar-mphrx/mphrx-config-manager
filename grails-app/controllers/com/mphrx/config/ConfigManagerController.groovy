package com.mphrx.config

import com.mphrx.Enum.ConfigDataType
import grails.converters.JSON
import groovy.json.JsonSlurper

class ConfigManagerController {

    def configurationService

    def update() {
        Map responseText = [:]
        try {
            def jsonSlurper = new JsonSlurper()
            Map configProperties = jsonSlurper.parseText(request.JSON.toString())
            responseText = configurationService.update(configProperties)
        } catch (Exception ex) {
            responseText = handleException(ex)
        }
        render responseText as JSON
    }

    /**
     * @return details of config that may contain one or more than one keys(closure)
     */
    def show() {
        def jsonSlurper = new JsonSlurper()
        Map configProperties = jsonSlurper.parseText(request.JSON.toString())
        AppConfig appConfig = AppConfig.get(configProperties.id)
        Map responseMap = appConfig.formattedOutput()
        responseMap.groupNameLabelCode = g.message(code: responseMap.groupNameLabelCode, default: responseMap.groupNameLabelCode)
        responseMap.logicalGroupLabelCode = g.message(code: responseMap.logicalGroupLabelCode, default: responseMap.logicalGroupLabelCode)
        responseMap.configMapList.each {
            it.keyLabelCode = g.message(code: it.keyLabelCode, default: it.keyLabelCode)
        }
        render responseMap as JSON
    }


    /**
     * @return name of configs grouped by their logical name
     */
    //TODO move to service
    def configList() {
        Map configMapLogical = [:]
        List configListLogical = []
        List<AppConfig> appConfigList = AppConfig.list()

        def appConfigListGroupByLogicalName = appConfigList.groupBy {
            it.logicalGroupLabelCode
        }

        appConfigListGroupByLogicalName.each {
            List configGroupList = []
            List subGroups=[]
            it.value.each { appConfig ->
                String description= g.message(code: appConfig.description,default: appConfig.description)
                Map appConfigDetails=[id: appConfig.id, groupName: g.message(code: appConfig.groupNameLabelCode, default: appConfig.groupNameLabelCode),description:description]
                if(appConfig.logicalSubGroupLabelCode){
                    String subGroupName= g.message(code: appConfig.logicalSubGroupLabelCode)
                    String subGroupDesc= g.message(code: appConfig.logicalSubGroupDescriptionLabelCode)
                    Map subGroup=subGroups.find{it.subGroupName ==subGroupName}

                    if(subGroup){
                        subGroup.appConfigs << appConfigDetails
                        subGroup.appConfigs?.sort{it.id}
                    }else{
                        subGroups<<[subGroupName: subGroupName,subGroupDesc:subGroupDesc,description:description,appConfigs:[appConfigDetails]]
                    }
                }else{
                    configGroupList << appConfigDetails
                }
            }
            configMapLogical.put(g.message(code: it.key, default: it.key), configGroupList)
            configListLogical << [logicalGroupName: g.message(code: it.key, default: it.key), appConfigs: configGroupList,subGroups:subGroups]
        }
        render configListLogical as JSON
    }

    /**
     * Load specific config from DB to memory.
     */
    def loadConfig() {
        Map responseText = [:]
        def jsonSlurper = new JsonSlurper()
        Map configProperties = jsonSlurper.parseText(request.JSON.toString())
        AppConfig appConfig = AppConfig.get(configProperties.id)
        responseText=configurationService.loadAppConfig(appConfig)
        render responseText as JSON
    }

    /**
     * Reads all configs from DB and loads into memory.
     */
    def loadAllConfigs() {
        Map responseText = ["msg": "Success", status: 200, httpStatusCode: 200]
        configurationService.loadAllConfigs()
        render responseText as JSON
    }

    def handleException(Exception ex) {
        Map responseText = [:]
        response.setStatus(500)
        responseText = ["msg": "Exception", status: 500]
        log.error("Exception occured ---- " + ex.printStackTrace())
        return responseText
    }

}
