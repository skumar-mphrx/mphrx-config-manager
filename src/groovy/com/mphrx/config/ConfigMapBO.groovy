package com.mphrx.config

import com.mphrx.Enum.ConfigDataType

/**
 * Created by shaurav on 24/6/15.
 */
class ConfigMapBO {

    String configkey
    String configValue
    List configValueList=[]
    Map configValueMap=[:]
    String keyLabelCode                   // message.properties entry of key for internationalization
    String description                   // description of key
    String defaultValue                  // each config must have a default value
    String dataType=ConfigDataType.String.value    // store datatype of config for validation
    List oldValues = []                  // [[value:'a',updateTime:'']]


}


