package com.mphrx.Enum

/**
 * Created by shaurav on 25/6/15.
 */
public enum ConfigDataType {


        Integer('Integer'),
        Float('Float'),
        String('String'),
        Boolean('Boolean'),
        List('List'),
        ValueSet('ValueSet'),  // also a list but value can be added or removed from this list
        Map('Map'),            // value can be modified only , no new item will be added
        Closure('Closure')

        private final String value;


        private ConfigDataType(String value) {
            this.value = value;
        }

        public String value() {
            return this.value;
        }
    }