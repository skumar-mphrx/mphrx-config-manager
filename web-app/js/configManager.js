/**
 * Created by shaurav on 29/6/15.
 */



var configManagerApp = angular.module('configManagerApp', ['ngRoute']).run(function () {
    console.log("App dependencies loaded.")
});

configManagerApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: "/minerva/configManager/listView",
        controller: 'configCtrl'
    }).when('/show/:id', {
        templateUrl: "/minerva/configManager/showView",
        controller: 'configCtrl'
    }).otherwise({
        redirectTo: '/'
    });
}]);

configManagerApp.controller('configCtrl', function ($scope, $http, $routeParams, $location) {

    $scope.fetchConfigList = function () {
        $http.post('/minerva/configManager/configList').success(function (res) {
            $scope.configList = res
        })
    }

    $scope.fetchConfigDetails = function () {
        $http.get('/minerva/configManager/show/' + $routeParams.id).success(function (res) {
            $scope.keyList = []
            $scope.appConfig = res
            $scope.appConfig.configMapList.forEach(function (config) {
                $scope.keyList.push({key: config.key, value: config.value})
            })
        })
    }

    $scope.updateConfig = function () {

        var configObject = {id: $scope.appConfig.id}

        $scope.keyList.forEach(function (obj) {
            var key = obj.key;
            configObject[key] = obj.value
        })

        console.log(configObject)
        console.log(JSON.stringify($scope.keyList))
        $http.post('/minerva/configManager/update', configObject).success(function (res) {
            console.log(res)
            $location.path('#list')
            console.log("Config updated successfully ... going back to list page ")
        })
    }


});
